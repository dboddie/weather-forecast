#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright (C) 2017 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import codecs, os, sys

import DUCK
from DUCK.Tools import buildhelper

def read_places():

    lines = codecs.open("data/noreg.txt", "r", "utf8").readlines()
    lines.pop(0)
    
    done = set()
    place_names = []
    latitudes = []
    longitudes = []
    altitudes = []
    place_spec = []
    
    for line in lines:

        pieces = line.strip().split("\t")
        place_type = pieces[3]
        if place_type == "By":
        
            name = pieces[1] + u", Norway"
            if name in done:
                continue
            
            done.add(name)
            
            lat, lon, alt = pieces[-6:-3]
            place_names.append(name)
            latitudes.append(lat)
            longitudes.append(lon)
            if alt == "": alt = "0"
            altitudes.append(alt)
            # For compatibility with existing user preferences.
            url = pieces[-1]
            place_spec.append(url[len("http://www.yr.no/place/"):-len("/forecast.xml")])

    lines = codecs.open("data/verda.txt", "r", "utf8").readlines()
    lines.pop(0)
    include_places = ['administration centre', 'airport', 'capital', 'city',
        'island', 'locality', 'populated locality', 'populated place',
        'regional capital', 'seat of government', 'town']
    
    for line in lines:
    
        pieces = line.strip().split("\t")
        place_type = pieces[7]
        if place_type in include_places:
            name = pieces[3] + u", %s" % pieces[10]
            if name in done:
                continue
            
            done.add(name)
            
            lat, lon, alt = pieces[-6:-3]
            place_names.append(name)
            latitudes.append(lat)
            longitudes.append(lon)
            if alt == "": alt = "0"
            altitudes.append(alt)
            # For compatibility with existing user preferences.
            url = pieces[-1]
            place_spec.append(url[len("http://www.yr.no/place/"):-len("/forecast.xml")])
    
    return place_names, latitudes, longitudes, altitudes, place_spec


app_name = "Weather Forecast"
package_name = "uk.org.boddie.android.weatherforecast"
version = "1.6.4"
version_code = "164"

symbols = set([
    "clearsky_day",
    "clearsky_night",
    "clearsky_polartwilight",
    "cloudy",
    "fair_day",
    "fair_night",
    "fair_polartwilight",
    "fog",
    "heavyrainandthunder",
    "heavyrain",
    "heavyrainshowersandthunder_day",
    "heavyrainshowersandthunder_night",
    "heavyrainshowersandthunder_polartwilight",
    "heavyrainshowers_day",
    "heavyrainshowers_night",
    "heavyrainshowers_polartwilight",
    "heavysleetandthunder",
    "heavysleet",
    "heavysleetshowersandthunder_day",
    "heavysleetshowersandthunder_night",
    "heavysleetshowersandthunder_polartwilight",
    "heavysleetshowers_day",
    "heavysleetshowers_night",
    "heavysleetshowers_polartwilight",
    "heavysnowandthunder",
    "heavysnow",
    "heavysnowshowersandthunder_day",
    "heavysnowshowersandthunder_night",
    "heavysnowshowersandthunder_polartwilight",
    "heavysnowshowers_day",
    "heavysnowshowers_night",
    "heavysnowshowers_polartwilight",
    "lightrainandthunder",
    "lightrain",
    "lightrainshowersandthunder_day",
    "lightrainshowersandthunder_night",
    "lightrainshowersandthunder_polartwilight",
    "lightrainshowers_day",
    "lightrainshowers_night",
    "lightrainshowers_polartwilight",
    "lightsleetandthunder",
    "lightsleet",
    "lightsleetshowers_day",
    "lightsleetshowers_night",
    "lightsleetshowers_polartwilight",
    "lightsnowandthunder",
    "lightsnow",
    "lightsnowshowers_day",
    "lightsnowshowers_night",
    "lightsnowshowers_polartwilight",
    "lightssleetshowersandthunder_day",
    "lightssleetshowersandthunder_night",
    "lightssleetshowersandthunder_polartwilight",
    "lightssnowshowersandthunder_day",
    "lightssnowshowersandthunder_night",
    "lightssnowshowersandthunder_polartwilight",
    "partlycloudy_day",
    "partlycloudy_night",
    "partlycloudy_polartwilight",
    "rainandthunder",
    "rain",
    "rainshowersandthunder_day",
    "rainshowersandthunder_night",
    "rainshowersandthunder_polartwilight",
    "rainshowers_day",
    "rainshowers_night",
    "rainshowers_polartwilight",
    "sleetandthunder",
    "sleet",
    "sleetshowersandthunder_day",
    "sleetshowersandthunder_night",
    "sleetshowersandthunder_polartwilight",
    "sleetshowers_day",
    "sleetshowers_night",
    "sleetshowers_polartwilight",
    "snowandthunder",
    "snow",
    "snowshowersandthunder_day",
    "snowshowersandthunder_night",
    "snowshowersandthunder_polartwilight",
    "snowshowers_day",
    "snowshowers_night",
    "snowshowers_polartwilight"
    ])

place_names, latitudes, longitudes, altitudes, place_spec = read_places()

res_files = {
    "drawable": {
        "ic_launcher": "images/svg/ic_launcher.svg",
        "clearsky_day": "images/png/clearsky_day.png",
        "clearsky_night": "images/png/clearsky_night.png",
        "clearsky_polartwilight": "images/png/clearsky_polartwilight.png",
        "cloudy": "images/png/cloudy.png",
        "fair_day": "images/png/fair_day.png",
        "fair_night": "images/png/fair_night.png",
        "fair_polartwilight": "images/png/fair_polartwilight.png",
        "fog": "images/png/fog.png",
        "heavyrainandthunder": "images/png/heavyrainandthunder.png",
        "heavyrain": "images/png/heavyrain.png",
        "heavyrainshowersandthunder_day": "images/png/heavyrainshowersandthunder_day.png",
        "heavyrainshowersandthunder_night": "images/png/heavyrainshowersandthunder_night.png",
        "heavyrainshowersandthunder_polartwilight": "images/png/heavyrainshowersandthunder_polartwilight.png",
        "heavyrainshowers_day": "images/png/heavyrainshowers_day.png",
        "heavyrainshowers_night": "images/png/heavyrainshowers_night.png",
        "heavyrainshowers_polartwilight": "images/png/heavyrainshowers_polartwilight.png",
        "heavysleetandthunder": "images/png/heavysleetandthunder.png",
        "heavysleet": "images/png/heavysleet.png",
        "heavysleetshowersandthunder_day": "images/png/heavysleetshowersandthunder_day.png",
        "heavysleetshowersandthunder_night": "images/png/heavysleetshowersandthunder_night.png",
        "heavysleetshowersandthunder_polartwilight": "images/png/heavysleetshowersandthunder_polartwilight.png",
        "heavysleetshowers_day": "images/png/heavysleetshowers_day.png",
        "heavysleetshowers_night": "images/png/heavysleetshowers_night.png",
        "heavysleetshowers_polartwilight": "images/png/heavysleetshowers_polartwilight.png",
        "heavysnowandthunder": "images/png/heavysnowandthunder.png",
        "heavysnow": "images/png/heavysnow.png",
        "heavysnowshowersandthunder_day": "images/png/heavysnowshowersandthunder_day.png",
        "heavysnowshowersandthunder_night": "images/png/heavysnowshowersandthunder_night.png",
        "heavysnowshowersandthunder_polartwilight": "images/png/heavysnowshowersandthunder_polartwilight.png",
        "heavysnowshowers_day": "images/png/heavysnowshowers_day.png",
        "heavysnowshowers_night": "images/png/heavysnowshowers_night.png",
        "heavysnowshowers_polartwilight": "images/png/heavysnowshowers_polartwilight.png",
        "lightrainandthunder": "images/png/lightrainandthunder.png",
        "lightrain": "images/png/lightrain.png",
        "lightrainshowersandthunder_day": "images/png/lightrainshowersandthunder_day.png",
        "lightrainshowersandthunder_night": "images/png/lightrainshowersandthunder_night.png",
        "lightrainshowersandthunder_polartwilight": "images/png/lightrainshowersandthunder_polartwilight.png",
        "lightrainshowers_day": "images/png/lightrainshowers_day.png",
        "lightrainshowers_night": "images/png/lightrainshowers_night.png",
        "lightrainshowers_polartwilight": "images/png/lightrainshowers_polartwilight.png",
        "lightsleetandthunder": "images/png/lightsleetandthunder.png",
        "lightsleet": "images/png/lightsleet.png",
        "lightsleetshowers_day": "images/png/lightsleetshowers_day.png",
        "lightsleetshowers_night": "images/png/lightsleetshowers_night.png",
        "lightsleetshowers_polartwilight": "images/png/lightsleetshowers_polartwilight.png",
        "lightsnowandthunder": "images/png/lightsnowandthunder.png",
        "lightsnow": "images/png/lightsnow.png",
        "lightsnowshowers_day": "images/png/lightsnowshowers_day.png",
        "lightsnowshowers_night": "images/png/lightsnowshowers_night.png",
        "lightsnowshowers_polartwilight": "images/png/lightsnowshowers_polartwilight.png",
        "lightssleetshowersandthunder_day": "images/png/lightssleetshowersandthunder_day.png",
        "lightssleetshowersandthunder_night": "images/png/lightssleetshowersandthunder_night.png",
        "lightssleetshowersandthunder_polartwilight": "images/png/lightssleetshowersandthunder_polartwilight.png",
        "lightssnowshowersandthunder_day": "images/png/lightssnowshowersandthunder_day.png",
        "lightssnowshowersandthunder_night": "images/png/lightssnowshowersandthunder_night.png",
        "lightssnowshowersandthunder_polartwilight": "images/png/lightssnowshowersandthunder_polartwilight.png",
        "partlycloudy_day": "images/png/partlycloudy_day.png",
        "partlycloudy_night": "images/png/partlycloudy_night.png",
        "partlycloudy_polartwilight": "images/png/partlycloudy_polartwilight.png",
        "rainandthunder": "images/png/rainandthunder.png",
        "rain": "images/png/rain.png",
        "rainshowersandthunder_day": "images/png/rainshowersandthunder_day.png",
        "rainshowersandthunder_night": "images/png/rainshowersandthunder_night.png",
        "rainshowersandthunder_polartwilight": "images/png/rainshowersandthunder_polartwilight.png",
        "rainshowers_day": "images/png/rainshowers_day.png",
        "rainshowers_night": "images/png/rainshowers_night.png",
        "rainshowers_polartwilight": "images/png/rainshowers_polartwilight.png",
        "sleetandthunder": "images/png/sleetandthunder.png",
        "sleet": "images/png/sleet.png",
        "sleetshowersandthunder_day": "images/png/sleetshowersandthunder_day.png",
        "sleetshowersandthunder_night": "images/png/sleetshowersandthunder_night.png",
        "sleetshowersandthunder_polartwilight": "images/png/sleetshowersandthunder_polartwilight.png",
        "sleetshowers_day": "images/png/sleetshowers_day.png",
        "sleetshowers_night": "images/png/sleetshowers_night.png",
        "sleetshowers_polartwilight": "images/png/sleetshowers_polartwilight.png",
        "snowandthunder": "images/png/snowandthunder.png",
        "snow": "images/png/snow.png",
        "snowshowersandthunder_day": "images/png/snowshowersandthunder_day.png",
        "snowshowersandthunder_night": "images/png/snowshowersandthunder_night.png",
        "snowshowersandthunder_polartwilight": "images/png/snowshowersandthunder_polartwilight.png",
        "snowshowers_day": "images/png/snowshowers_day.png",
        "snowshowers_night": "images/png/snowshowers_night.png",
        "snowshowers_polartwilight": "images/png/snowshowers_polartwilight.png"
        },
    "raw": {
        "sample": "tests/oslo.json"
        },
    "string": {
        "show_detailed": "Show detailed forecast",
        "api_met_no_data": "Data from api.met.no",
        "add_location": "Add",
        "configure_app": "Configure",
        "remove_location": "Remove",
        "cancel": "Cancel",
        "place_name": "Place Name",
        "date": "Date",
        "time": "Time",
        "temperature": "Temperature",
        "wind_speed": "Wind speed",
        "insecure_protocol": "Cannot use a secure protocol.\nFalling back to insecure networking.",
        "failed_to_read": "Failed to read weather forecast."
        },
    "string-nb": {
        "show_detailed": u"Vis detaljert varsel",
        "api_met_no_data": u"Data fra api.met.no",
        "add_location": u"Legg til",
        "configure_app": u"Tilpass",
        "remove_location": u"Fjern",
        "cancel": u"Avbryt",
        "place_name": u"Stedsnavn",
        "date": u"Dato",
        "time": u"Tidspunkt",
        "temperature": u"Temperatur",
        "wind_speed": u"Vindhastighet",
        "insecure_protocol": u"Kan ikke bruke en sikker protokoll.\nSkal bruke usikret nettverk.",
        "failed_to_read": u"Kan ikke lese værvarsel."
        },
    "values": {
        "symbols": sorted(symbols),
        "place_names": place_names,
        "latitudes": latitudes,
        "longitudes": longitudes,
        "altitudes": altitudes,
        "places": place_spec
        }
    }

# Store the resource IDs that will be allocated for each of the above drawables
# in a list that can be accessed at run time. This can be cross-referenced with
# the sorted set of symbols because resources are sorted by their keys before
# being encoded in the application's resources.
ids = []

for x, key in enumerate(sorted(res_files["drawable"].keys())):
    if key in symbols:
        # Only provide IDs for symbols, not other icons like the launcher icon.
        ids.append(0x7f010000 | x)

res_files["values"]["resourceIDs"] = ids

# Define general information about the application.
code_file = "src/weatherforecast.py"
include_paths = ["src"]
layout = None
features = []
permissions = ["android.permission.INTERNET",
               "android.permission.READ_EXTERNAL_STORAGE",
               "android.permission.WRITE_EXTERNAL_STORAGE"]

# General customisation options, though these specific ones are related to icon
# generation. Enable the icon cache for faster package creation when developing.
options = {"pngquant": "-f 32",
           "icon cache": False}

if __name__ == "__main__":

    args = sys.argv[:]
    
    result = buildhelper.main(__file__, app_name, package_name, res_files,
        layout, code_file, include_paths, features, permissions, args,
        include_sources = False, sdk_version = 9, options = options,
        version = version)
    
    sys.exit(result)
