Weather Forecast
================

This application downloads and displays weather forecasts available from the
api.met.no service.

It is available to install from the F-Droid catalogue:
[![Current package version](https://img.shields.io/f-droid/v/uk.org.boddie.android.weatherforecast.svg)](https://www.f-droid.org/en/packages/uk.org.boddie.android.weatherforecast/)

![A screenshot of the application](docs/screenshot.png)

See the [Changelog](Changelog) for details about each version of this
application. **Note that version 1.2.0 and later may have issues with handling
secure communication with the server, particularly for older Android devices,
running versions earlier than Android 5 (Lollipop).**

Adding, removing and selecting locations
----------------------------------------

The application is not configured to use any default locations, but a list of
preset locations are supplied. Select the text field at the bottom of the
screen, start entering a place name, and suggestions for the location will be
presented. You can optionally select one of the suggestions. Press the Add
button to add the location to the list. Since the list of known locations
is limited, your preferred location may not be available; see below for a
workaround for this problem.

To remove a location from the list, press and hold the relevant item in the
list until the Remove and Cancel buttons appear below the list. Press Remove to
remove the item or Cancel to keep it.

To view a forecast for a location, tap the relevant item. If a connection to
the api.met.no server can be established, the location list will be hidden and
the forecast will be shown. Otherwise, a "No connection" message will be
displayed.

Adding custom locations
-----------------------

The app contains a list of known locations which I obtained from the yr.no
service, which is no longer available. The service provided information about
many more places than these. If your preferred location is not recognised by
the application then the following workaround can be used to add it to your
list of locations.

The app stores its locations in a file in the external storage of your device.
If you use a file browser on your phone, you can find a file called
`locations.txt` in the `Download/WeatherForecast` directory. For example, it
might be found in the following location:

  SD Card
    Download
      WeatherForecast
        locations.txt

The corresponding path is `/sdcard/Download/WeatherForecast/locations.txt` if
you access it via a shell.

The `locations.txt` file contains a list of names and geographic coordinates
for each location, with one location per line, separated by spaces. For
example, the line describing Tokyo looks like this:

  Tokyo 35.68950 139.69171 40

The name of the location is followed by the latitude and longitude, measured in
degrees. The last value is the altitude/elevation, measured in metres. You can
also include spaces in the name:

  Melbourne, Australia -37.81501 144.96657 32

Exit the application and ensure that it is not running. Then update the file to
include the custom locations you want and restart the application. The new
locations should now be available.

Building the application
------------------------

The application is written in the Serpentine language which requires the
[DUCK](http://www.boddie.org.uk/david/Projects/Python/DUCK/README.html)
software to be installed or on the `PYTHONPATH`. Additionally, you will need
to have created a key and signing certificate in order to create an installable
package unless you are using a separate signing process.

The current version of this application (1.6.4) has been tested to build with
DUCK version 1.1.0.

The `build.py` script supplied with the application can then be run in the
following way, where `<key.pem>` and `<cert.pem>` are the locations of your
key and certificate files:

```\
./build.py <key.pem> <cert.pem> WeatherForecast.apk
```

You can then install the package on your Android device in the usual way.

Alternatively, if you are using a separate signing process, you can build the
package without signing it in the following way:

```\
./build.py WeatherForecast.apk
```

Updating version numbers
------------------------

When making a new release, the version number of the application needs to be
updated. This value is stored in two places: in the previous section of this
document and in the `build.py` file. The value used in the build file will be
stored in the application's manifest. It will also be used by the F-Droid
build process to ensure that the package version reflects the stated current
version.

If the application is updated to require features of a later version of DUCK
then the previous section should be updated to specify the version number of
DUCK that is required and the same version number should be given in the
`deps/DUCK` file in this repository.

The metadata for this application in the
[F-Droid Data repository](https://gitlab.com/fdroid/fdroiddata/) can be
updated to ensure that the correct version of DUCK is used to build the
application, but the use of the `deps/DUCK` file should make that task
unnecessary.

About the api.met.no service and data
-------------------------------------

The weather forecasts are obtained from the service at api.met.no and the
application tries to follow the terms, conditions and guidelines for use of
that service:

https://www.met.no/en/free-meteorological-data/Licensing-and-crediting

The place names included with the application were obtained from the following
locations:

http://fil.nrk.no/yr/viktigestader/noreg.txt
http://fil.nrk.no/yr/viktigestader/verda.txt

This information is presumably provided under the terms of the Creative Commons
Attribution 4.0 International (CC BY 4.0) license, as used by the geonames.org
service for the data the noreg.txt and verda.txt files are based on.

The symbols supplied were obtained from the following repository and are
licensed under the MIT license:

https://api.met.no/weatherapi/weathericon/2.0/data

License
-------

The source code is licensed under the GNU General Public License version 3 or
later. See the COPYING file for more information about this license. A short
version of the license is given below:

Copyright (C) 2017 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
