"""
networking.py - Networking code for the Weather Forecast application.

Copyright (C) 2019 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from java.lang import Object, String, System
from java.net import InetAddress, Socket
from javax.net.ssl import HttpsURLConnection, SSLContext, SSLSocket, SSLSocketFactory
from android.util import Log


### Experimental socket factory that uses TLSv1.2 to communicate with the yr.no
### server. This is apparently needed on Android 4 <= version < 5 due to a
### bug in protocol selection: https://issuetracker.google.com/issues/37043001
### yr.no has disabled SSLv3 and earlier TLS versions, so we have to use this
### version.

class TLSSocketFactory(SSLSocketFactory):

    def __init__(self):
        SSLSocketFactory.__init__(self)
        
        self.context = SSLContext.getInstance("TLSv1.2")
        self.context.init(None, None, None)
        self._socketFactory = SSLContext.getDefault().getSocketFactory()
    
    @args(Socket, [SSLSocket])
    def useTLS(self, socket):
    
        Log.i("DUCK", "UseTLS")
        for name in socket.getEnabledProtocols():
            Log.i("DUCK", name)
        socket.setEnabledProtocols(array(["TLSv1.2"]))
        
        return socket
    
    @args(Socket, [String, int, InetAddress, int])
    def createSocket(self, host, port, localHost, localPort):
        Log.i("DUCK", "createSocket String, int, InetAddress, int")
        socket = CAST(
            self._socketFactory.createSocket(host, port, localHost, localPort),
            SSLSocket
            )
        return self.useTLS(socket)
    
    @args(Socket, [InetAddress, int, InetAddress, int])
    def createSocket(self, address, port, localAddress, localPort):
        Log.i("DUCK", "createSocket InetAddress, int, InetAddress, int")
        socket = CAST(
            self._socketFactory.createSocket(address, port, localAddress, localPort),
            SSLSocket
            )
        return self.useTLS(socket)
    
    @args(Socket, [InetAddress, int])
    def createSocket(self, host, port):
        Log.i("DUCK", "createSocket InetAddress, int")
        socket = CAST(
            self._socketFactory.createSocket(host, port), SSLSocket
            )
        return self.useTLS(socket)
    
    @args(Socket, [String, int])
    def createSocket(self, host, port):
        Log.i("DUCK", "createSocket String, int")
        socket = CAST(
            self._socketFactory.createSocket(host, port), SSLSocket
            )
        return self.useTLS(socket)
    
    def createSocket(self):
        Log.i("DUCK", "createSocket")
        socket = CAST(self._socketFactory.createSocket(), SSLSocket)
        return self.useTLS(socket)
    
    @args(Socket, [Socket, String, int, bool])
    def createSocket(self, s, host, port, autoClose):
        Log.i("DUCK", "createSocket Socket, String, int, bool")
        socket = CAST(
            self._socketFactory.createSocket(s, host, port, autoClose),
            SSLSocket
            )
        return self.useTLS(socket)
    
    def getDefaultCipherSuites(self):
        Log.i("DUCK", "getDefaultCipherSuites")
        return self._socketFactory.getDefaultCipherSuites()
    
    def getSupportedCipherSuites(self):
        Log.i("DUCK", "getSupportedCipherSuites")
        return self._socketFactory.getSupportedCipherSuites()
