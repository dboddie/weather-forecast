"""
forecastparser.py - An XML parser for the Weather Forecast application.

Copyright (C) 2017 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from java.io import ByteArrayOutputStream, InputStream
from java.lang import Object, String
from java.text import DateFormat, ParsePosition, SimpleDateFormat
from java.util import Date, HashMap, List, TimeZone
from android.content.res import Resources
from android.view import View
from org.json import JSONArray, JSONObject, JSONTokener

from app_resources import R

class ForecastParser(Object):

    @args(void, [HashMap(String, int)])
    def __init__(self, symbols):
    
        Object.__init__(self)
        self.symbols = symbols
    
    @args(List(Forecast), [InputStream])
    def parse(self, stream):
    
        # https://stackoverflow.com/questions/309424/how-do-i-read-convert-an-inputstream-into-a-string-in-java
        buf_out = ByteArrayOutputStream()
        
        while True:
            b = stream.read()
            if b == -1:
                break
            buf_out.write(b)
        
        json_text = buf_out.toString("UTF-8")
        
        dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"))
        
        forecasts = []
        
        obj = CAST(JSONTokener(json_text).nextValue(), JSONObject)
        
        properties = obj.getJSONObject("properties")
        units = properties.getJSONObject("meta").getJSONObject("units")
        timeseries = properties.getJSONArray("timeseries")
        
        t = 0
        while t < len(timeseries):
        
            f = timeseries.getJSONObject(t)
            
            forecast = Forecast()
            time_text = f.getString("time")
            forecast.date = dateFormat.parse(time_text, ParsePosition(0))
            
            data = f.getJSONObject("data")
            
            if data.has("instant"):
            
                instant = data.getJSONObject("instant")
                details = instant.getJSONObject("details")
                forecast.windSpeed = details.getString("wind_speed")
                forecast.windUnit = units.getString("wind_speed")
                forecast.temperature = details.getString("air_temperature")
                forecast.temperatureUnit = units.getString("air_temperature")
            
            if data.has("next_1_hours"):
                summary = data.getJSONObject("next_1_hours").getJSONObject("summary")
                forecast.length = 1
            elif data.has("next_6_hours"):
                summary = data.getJSONObject("next_6_hours").getJSONObject("summary")
                forecast.length = 6
            elif data.has("next_12_hours"):
                summary = data.getJSONObject("next_12_hours").getJSONObject("summary")
                forecast.length = 12
            else:
                summary = None
                forecast.symbol = -1
            
            if summary != None:
                symbol = summary.getString("symbol_code")
                try:
                    forecast.symbol = self.symbols[symbol]
                except KeyError:
                    forecast.symbol = -1
            
            forecasts.add(forecast)
            t += 1
        
        return forecasts


class Forecast(Object):

    __fields__ = {
        "date": Date,
        "symbol": int,
        "description": String,
        "windSpeed": String,
        "windUnit": String,
        "temperature": String,
        "temperatureUnit": String,
        "length": int
        }
    
    def __init__(self):
        Object.__init__(self)


class Forecasts(Object):

    __fields__ = {
        "forecasts": List(Forecast)
        }
    
    def __init__(self):
        Object.__init__(self)
        self.forecasts = []
    
    @args(void, [List(Forecast)])
    def __init__(self, forecasts):
        Object.__init__(self)
        self.forecasts = forecasts
