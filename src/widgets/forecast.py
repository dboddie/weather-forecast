"""
forecastwidget.py - A forecast widget for the Weather Forecast application.

Copyright (C) 2017 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from java.lang import Float, Math, Object, String
from java.util import Calendar, Date, List, Locale, Map
from android.content import Context, SharedPreferences
from android.graphics import Color, Typeface
from android.os import Build
from android.view import Gravity, View, ViewGroup
from android.widget import CheckBox, CompoundButton, ImageView, LinearLayout, \
    ListView, RelativeLayout, ScrollView, Space, Switch, TextView

import android.R
from app_resources import R

from serpentine.widgets import VBox

from forecastparser import Forecast


class ForecastWidget(RelativeLayout):

    __fields__ = {
        "forecasts": List(Forecast),
        "detailedForecastBox": CompoundButton
        }
    
    def __init__(self, context):
    
        RelativeLayout.__init__(self, context)
        
        self.place_name = ""
        self.forecasts = None
        
        # This getColor call deprecated in API level 23.
        background = context.getResources().getColor(android.R.color.background_light)
        
        # Header
        self.header = Header(context)
        
        if Build.VERSION.SDK_INT < 14:
            self.detailedForecastBox = CheckBox(context)
        else:
            self.detailedForecastBox = Switch(context)
        
        self.detailedForecastBox.setText(R.string.show_detailed)
        
        headerLine = View(context)
        headerLine.setBackgroundColor(background)
        headerLineParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT, 1) # 1 pixel in height
        
        headerLayout = VBox(context)
        headerLayout.setId(1)
        headerLayout.addView(self.header)
        headerLayout.addView(headerLine, headerLineParams)
        
        # Middle - containing the forecast layout
        self.scrollView = ScrollView(context)
        self.scrollView.setId(2)
        
        # Footer
        footer = LinearLayout(context)
        footer.setOrientation(LinearLayout.VERTICAL)
        footer.setId(3)
        
        footerLine = View(context)
        footerLine.setBackgroundColor(background)
        footerLineParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT, 1) # 1 pixel in height
        
        self.creditLabel = TextView(context)
        
        footer.addView(footerLine, footerLineParams)
        footer.addView(self.creditLabel)
        footer.addView(self.detailedForecastBox)
        
        # The forecast layout
        self.forecastLayout = LinearLayout(context)
        self.forecastLayout.setOrientation(LinearLayout.VERTICAL)
        self.scrollView.addView(self.forecastLayout)
        
        # Layout parameters
        headerParams = RelativeLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT)
        headerParams.addRule(RelativeLayout.ALIGN_PARENT_TOP)
        headerParams.addRule(RelativeLayout.CENTER_HORIZONTAL)
        
        scrollParams = RelativeLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT)
        scrollParams.addRule(RelativeLayout.CENTER_HORIZONTAL)
        scrollParams.addRule(RelativeLayout.BELOW, 1)
        scrollParams.addRule(RelativeLayout.ABOVE, 3)
        
        footerParams = RelativeLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT)
        footerParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM)
        
        self.addView(headerLayout, headerParams)
        self.addView(self.scrollView, scrollParams)
        self.addView(footer, footerParams)
    
    @args(void, [String, List(Forecast), SharedPreferences])
    def addForecasts(self, place_name, forecasts, preferences):
    
        if len(forecasts) == 0:
            return
        
        forecast_period = preferences.getInt("forecast period", 6)
        
        context = self.getContext()
        
        self.forecastLayout.removeAllViews()
        self.scrollView.scrollTo(0, 0)
        
        self.header.setText(place_name)
        self.creditLabel.setText(R.string.api_met_no_data)
        
        firstDate = forecasts[0].date
        calendar = Calendar.getInstance()
        calendar.setTime(firstDate)
        
        # Track the current day so that separators can be inserted between them.
        currentDay = calendar.get(Calendar.DAY_OF_MONTH)
        
        # Accumulate forecasts until at least a six hour window has been covered.
        covered = 0
        current = Forecast()
        
        for forecast in forecasts:
        
            # Get the day of the month of the forecast.
            date = forecast.date
            calendar.setTime(date)
            day = calendar.get(Calendar.DAY_OF_MONTH)
            
            # Add an item for the date for the first item and any item
            # following a day change.
            if date == firstDate or day != currentDay:
            
                # Add any pending forecast.
                if covered > 0:
                    self.addForecast(context, preferences, current)
                    covered = 0
                
                # Add a date item.
                dateWidget = DateWidget(context, calendar, day)
                dateWidget.restore(preferences)
                self.forecastLayout.addView(dateWidget, self.rowLayout())
            
            currentDay = day
            
            # Use the first forecast in each six hour period.
            if covered == 0:
                current = forecast
            
            covered += forecast.length
            
            # Show a forecast if long enough window has been covered.
            if covered >= forecast_period:
                self.addForecast(context, preferences, current)
                covered = 0
        
        self.place_name = place_name
        self.forecasts = forecasts
    
    @args(void, [Context, SharedPreferences, Forecast])
    def addForecast(self, context, preferences, forecast):
    
        # Time
        timeWidget = TimeWidget(context, forecast.date)
        timeWidget.restore(preferences)
        self.forecastLayout.addView(timeWidget, self.rowLayout())
        
        # Symbol, temperature, description and wind
        symbolWidget = SymbolWidget(context, forecast)
        symbolWidget.tempWidget.restore(preferences)
        symbolWidget.windWidget.restore(preferences)
        self.forecastLayout.addView(symbolWidget, self.rowLayout())
    
    @args(LinearLayout.LayoutParams, [])
    def rowLayout(self):
    
        return LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                         ViewGroup.LayoutParams.WRAP_CONTENT)
    
    @args(RelativeLayout.LayoutParams, [])
    def itemLayout(self):
    
        return RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                                           ViewGroup.LayoutParams.WRAP_CONTENT)
    
    @args(void, [SharedPreferences])
    def restore(self, preferences):
    
        self.header.restore(preferences)
        
        detailed = preferences.getInt("forecast period", 6) == 1
        self.detailedForecastBox.setChecked(detailed)
        
        if self.forecasts != None:
            self.addForecasts(self.place_name, self.forecasts, preferences)


class Adjustable:

    @args(int, [])
    def getAdjustment(self):
        pass
    
    @args(void, [int])
    def setAdjustment(self, size):
        pass


class AdjustableText(TextView):

    __interfaces__ = [Adjustable]
    
    __fields__ = {
        "adjustment": int,
        "defaultSize": int
        }
    
    @args(void, [Context, str])
    def __init__(self, context, preferenceString):
    
        TextView.__init__(self, context)
        self.setGravity(Gravity.CENTER)
        
        self.preferenceString = preferenceString
        
        # Record the original text size for further adjustment.
        self.originalTextSize = self.getTextSize()
    
    def getAdjustment(self):
    
        return self.adjustment
    
    def setAdjustment(self, adjustment):
    
        self.adjustment = adjustment
        value = Math.max(0, self.originalTextSize + self.adjustment)
        self.setTextSize(value)
    
    @args(void, [SharedPreferences])
    def restore(self, preferences):
    
        adjustment = preferences.getInt(self.preferenceString, self.defaultSize)
        self.setAdjustment(adjustment)
    
    @args(void, [SharedPreferences.Editor])
    def save(self, editor):
    
        editor.putInt(self.preferenceString, self.adjustment)


class Header(AdjustableText):

    def __init__(self, context):
    
        AdjustableText.__init__(self, context, "header adjustment")
        self.defaultSize = 6


class DateWidget(AdjustableText):

    @args(void, [Context, Calendar, int])
    def __init__(self, context, calendar, day):
    
        AdjustableText.__init__(self, context, "date adjustment")
        
        background = context.getResources().getColor(android.R.color.background_light)
        
        self.setText(
            calendar.getDisplayName(Calendar.DAY_OF_WEEK,
                Calendar.LONG, Locale.getDefault()) + " " + \
            str(day) + " " + \
            calendar.getDisplayName(Calendar.MONTH,
                Calendar.LONG, Locale.getDefault()) + " " + \
            str(calendar.get(Calendar.YEAR)))
        
        self.setGravity(Gravity.CENTER)
        self.setTypeface(Typeface.create(None, Typeface.BOLD))
        self.setBackgroundColor(background)
        self.setTextColor(0xff000000)
        self.defaultSize = 3


class TimeWidget(AdjustableText):

    @args(void, [Context, Date])
    def __init__(self, context, date):
    
        AdjustableText.__init__(self, context, "time adjustment")
        
        calendar = Calendar.getInstance()
        calendar.setTime(date)
        
        timeString = String.format("%02d:%02d",
            array([calendar.get(Calendar.HOUR_OF_DAY),
                   calendar.get(Calendar.MINUTE)]))
        
        self.setText(timeString)
        
        self.setGravity(Gravity.CENTER)
        self.setTypeface(Typeface.create(None, Typeface.BOLD))
        self.defaultSize = 3


class SymbolWidget(RelativeLayout):

    __fields__ = {"tempWidget": TemperatureWidget,
                  "windWidget": WindWidget}
    
    @args(void, [Context, Forecast])
    def __init__(self, context, forecast):
    
        RelativeLayout.__init__(self, context)
        
        # Temperature Symbol Wind speed
        
        # Temperature
        self.tempWidget = TemperatureWidget(context)
        tempUnit = forecast.temperatureUnit
        
        if tempUnit == "celsius":
            tempUnit = u"\u2103"
        
        self.tempWidget.setText(forecast.temperature + tempUnit)
        
        lp = self.itemLayout()
        lp.addRule(RelativeLayout.CENTER_VERTICAL)
        lp.addRule(RelativeLayout.ALIGN_PARENT_LEFT)
        self.addView(self.tempWidget, lp)
        
        # Symbol
        lp = self.itemLayout()
        lp.addRule(RelativeLayout.CENTER_IN_PARENT)
        
        if forecast.symbol != -1:
            imageView = ImageView(context)
            imageView.setImageResource(forecast.symbol)
            self.addView(imageView, lp)
        elif Build.VERSION.SDK_INT >= 14:
            spacer = Space(context)
            self.addView(spacer, lp)
        
        # Wind speed
        self.windWidget = WindWidget(context)
        self.windWidget.setText(forecast.windSpeed + " " + forecast.windUnit)
        
        lp = self.itemLayout()
        lp.addRule(RelativeLayout.CENTER_VERTICAL)
        lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT)
        self.addView(self.windWidget, lp)
    
    @args(RelativeLayout.LayoutParams, [])
    def itemLayout(self):
    
        return RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                                           ViewGroup.LayoutParams.WRAP_CONTENT)


class TemperatureWidget(AdjustableText):

    def __init__(self, context):
    
        AdjustableText.__init__(self, context, "temperature adjustment")
        self.defaultSize = 6


class WindWidget(AdjustableText):

    def __init__(self, context):
    
        AdjustableText.__init__(self, context, "wind speed adjustment")
        self.defaultSize = 3
