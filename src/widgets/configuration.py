"""
configuration.py - Configuration widgets for the Weather Forecast application.

Copyright (C) 2019 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from java.lang import String
from java.util import Calendar, HashMap

from android.content import Context, SharedPreferences
from android.os import Build
from android.view import Gravity, View, ViewGroup
from android.widget import Button, LinearLayout, RelativeLayout, ScrollView, \
    Space, TextView

import android.R
from app_resources import R

from serpentine.widgets import HBox, VBox

from forecast import Adjustable, DateWidget, Header, SymbolWidget, TimeWidget
from forecastparser import Forecast


class ConfigureListener:

    def startConfiguration(self):
        pass
    
    # This method is used to connect any controls in the configuration page
    # to the activity. At the moment there are no controls that call it.
    @args(void, [bool])
    def finishConfiguration(self, save):
        pass
    
    def restorePreferences(self):
        pass
    
    def savePreferences(self):
        pass


class ConfigureWidget(RelativeLayout):

    __fields__ = {
        "header": Header,
        "dateWidget": DateWidget,
        "timeWidget": TimeWidget,
        "symbolWidget": SymbolWidget
        }
    
    @args(void, [Context, HashMap(String, int), ConfigureListener])
    def __init__(self, context, symbols, listener):
    
        RelativeLayout.__init__(self, context)
        
        self.symbols = symbols
        self.listener = listener
        
        self.background = context.getResources().getColor(android.R.color.background_light)
        
        headerLine = View(context)
        headerLine.setBackgroundColor(self.background)
        headerLineParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT, 1) # 1 pixel in height
        
        headerLayout = VBox(context)
        headerLayout.setId(1)
        headerLayout.addView(headerLine, headerLineParams)
        
        # Middle
        
        self.middleLayout = VBox(context)
        self.middleLayout.setId(2)
        self.updateSample()
        
        # Overall footer layout
        
        # Add controls to allow the sizes of the labels and symbol to be
        # customised.
        scrollView = ScrollView(context)
        scrollLayout = VBox(context)
        self.headerControl = self.addSizeControl(R.string.place_name, None, scrollLayout)
        self.dateControl = self.addSizeControl(R.string.date, None, scrollLayout)
        self.timeControl = self.addSizeControl(R.string.time, None, scrollLayout)
        self.tempControl = self.addSizeControl(R.string.temperature, None, scrollLayout)
        self.windControl = self.addSizeControl(R.string.wind_speed, None, scrollLayout)
        self.updateControls()
        scrollView.addView(scrollLayout)
        scrollView.setId(3)
        
        # Layout parameters
        headerParams = RelativeLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT)
        headerParams.addRule(RelativeLayout.ALIGN_PARENT_TOP)
        headerParams.addRule(RelativeLayout.CENTER_HORIZONTAL)
        
        middleParams = RelativeLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT)
        middleParams.addRule(RelativeLayout.CENTER_HORIZONTAL)
        middleParams.addRule(RelativeLayout.BELOW, 1)
        
        footerParams = RelativeLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT)
        footerParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM)
        footerParams.addRule(RelativeLayout.BELOW, 2)
        
        self.addView(headerLayout, headerParams)
        self.addView(self.middleLayout, middleParams)
        self.addView(scrollView, footerParams)
    
    @args(void, [SharedPreferences])
    def restore(self, preferences):
    
        self.header.restore(preferences)
        self.dateWidget.restore(preferences)
        self.timeWidget.restore(preferences)
        self.symbolWidget.tempWidget.restore(preferences)
        self.symbolWidget.windWidget.restore(preferences)
        self.updateControls()
    
    @args(void, [SharedPreferences.Editor])
    def save(self, editor):
    
        self.header.save(editor)
        self.dateWidget.save(editor)
        self.timeWidget.save(editor)
        self.symbolWidget.tempWidget.save(editor)
        self.symbolWidget.windWidget.save(editor)
    
    @args(SizeControl, [int, Adjustable, VBox])
    def addSizeControl(self, resId, adjustable, layout):
    
        label = TextView(self.getContext())
        label.setText(resId)
        label.setGravity(Gravity.CENTER)
        
        sizeControl = SizeControl(self.getContext(), label.getTextSize(),
                                  adjustable, self)
        layout.addView(label)
        layout.addView(sizeControl)
        
        return sizeControl
    
    def updateSample(self):
    
        context = self.getContext()
        
        # Include a place name header, date, time and a forecast to allow the
        # user to configure the appearance of the forecast view.
        self.header = Header(context)
        self.header.setText(R.string.place_name)
        
        calendar = Calendar.getInstance()
        day = calendar.get(Calendar.DAY_OF_MONTH)
        
        forecast = Forecast()
        forecast.date = calendar.getTime()
        forecast.symbol = self.symbols["rain"]
        forecast.temperatureUnit = "celsius"
        forecast.temperature = "12"
        forecast.windSpeed = "3"
        forecast.windUnit = "m/s"
        
        self.dateWidget = DateWidget(context, calendar, day)
        self.timeWidget = TimeWidget(context, forecast.date)
        self.symbolWidget = SymbolWidget(context, forecast)
        
        centreParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT)
        
        footerLine = View(context)
        footerLine.setBackgroundColor(self.background)
        footerLineParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT, 1) # 1 pixel in height
        
        spacers = Build.VERSION.SDK_INT >= 14
        
        self.middleLayout.removeAllViews()
        if spacers:
            self.middleLayout.addView(Space(context), 0, 12)
        self.middleLayout.addView(self.header, centreParams)
        self.middleLayout.addView(self.dateWidget, centreParams)
        self.middleLayout.addView(self.timeWidget, centreParams)
        self.middleLayout.addView(self.symbolWidget, centreParams)
        if spacers:
            self.middleLayout.addView(Space(context), 0, 12)
        self.middleLayout.addView(footerLine, footerLineParams)
        if spacers:
            self.middleLayout.addView(Space(context), 0, 12)
    
    def updateControls(self):
    
        self.headerControl.setAdjustable(self.header)
        self.dateControl.setAdjustable(self.dateWidget)
        self.timeControl.setAdjustable(self.timeWidget)
        self.tempControl.setAdjustable(self.symbolWidget.tempWidget)
        self.windControl.setAdjustable(self.symbolWidget.windWidget)
    
    def updateConfiguration(self):
    
        # Save the preferences, recreate the sample forecast and the controls
        # for its widgets, then apply the preferences to the widgets.
        self.listener.savePreferences()
        self.updateSample()
        self.listener.restorePreferences()


class SizeControl(HBox):

    __interfaces__ = [View.OnClickListener]
    
    @args(void, [Context, float, Adjustable, ConfigureWidget])
    def __init__(self, context, baseSize, adjustable, configWidget):
    
        HBox.__init__(self, context)
        self.setGravity(Gravity.CENTER)
        
        self.adjustable = adjustable
        self.configWidget = configWidget
        
        self.adjustment = 0
        
        step = baseSize / 4.0
        
        # Create labels to decrease and increase the font size, using the ID to
        # store information about the font size associated with the label.
        minusButton = Button(context)
        minusButton.setText("-")
        minusButton.setId(0x10 - 1)
        minusButton.setOnClickListener(self)
        self.addView(minusButton)
        
        plusButton = Button(context)
        plusButton.setText("+")
        plusButton.setId(0x10 + 1)
        plusButton.setOnClickListener(self)
        self.addView(plusButton)
    
    def onClick(self, view):
    
        self.adjustment += view.getId() - 0x10
        self.adjustable.setAdjustment(self.adjustment)
        self.configWidget.updateConfiguration()
    
    @args(void, [Adjustable])
    def setAdjustable(self, adjustable):
    
        self.adjustable = adjustable
        self.adjustment = adjustable.getAdjustment()
