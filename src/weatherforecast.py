"""
weatherforecast.py - Application code for the Weather Forecast application.

Copyright (C) 2017 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from java.io import BufferedInputStream, InputStream
from java.lang import Exception, Object, String, System
from java.net import HttpURLConnection, URL
from java.util import List, Map
from javax.net.ssl import HttpsURLConnection, SSLException
from android.os import AsyncTask, Build
from android.util import Log
from android.view import View
from android.widget import Toast
from serpentine.activities import Activity

from app_resources import R

from exceptions import WeatherException
from forecastparser import Forecast, Forecasts, ForecastParser
from widgets.configuration import ConfigureListener, ConfigureWidget
from widgets.forecast import ForecastWidget
from widgets.location import Coordinates, LocationListener, LocationWidget
                    
from networking import TLSSocketFactory


class WeatherForecastActivity(Activity):

    __interfaces__ = [ConfigureListener, LocationListener, View.OnClickListener]
    
    __fields__ = {
        "cache": Map(Coordinates, CacheItem),
        "socketFactory": TLSSocketFactory
        }
    
    def __init__(self):
    
        Activity.__init__(self)
        self.state = "entry"
        self.cache = {}
    
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        resources = self.getResources()
        
        # Obtain the keys and values to be used to create the symbols
        # dictionary from the application's resources.
        symbols = resources.getStringArray(R.array.symbols)
        resourceIDs = resources.getIntArray(R.array.resourceIDs)
        
        self.symbols = dict(symbols, resourceIDs)
        self.parser = ForecastParser(self.symbols)
        
        # Map place specifications to coordinates so that when another component
        # provides a specification, it can be used to obtain a forecast.
        places = resources.getStringArray(R.array.places)
        latitudes = resources.getStringArray(R.array.latitudes)
        longitudes = resources.getStringArray(R.array.longitudes)
        altitudes = resources.getStringArray(R.array.altitudes)
        
        self.coordinates = {}
        
        i = 0
        while i < len(latitudes):
            c = Coordinates(latitudes[i], longitudes[i], altitudes[i])
            self.coordinates[places[i].toLowerCase()] = c
            i += 1
        
        self.entryWidget = LocationWidget(self, self, self, self.coordinates)
        self.configureWidget = ConfigureWidget(self, self.symbols, self)
        self.setContentView(self.entryWidget)
        
        # By default, don't use a custom socket factory. We need one to handle
        # communications for versions of Android before Lollipop (5), apparently.
        self.socketFactory = None
        
        try:
            if 16 <= Build.VERSION.SDK_INT < 21:
                # For versions with possible TLSv1.2 support, use a custom
                # socket factory.
                self.socketFactory = TLSSocketFactory()
                HttpsURLConnection.setDefaultSSLSocketFactory(self.socketFactory)
            
            elif Build.VERSION.SDK_INT < 16:
                # We cannot use TLSv1.2 with earlier versions.
                raise ValueError()
        except:
            # Tell the user about the lack of secure networking.
            Toast.makeText(self, R.string.insecure_protocol,
                           Toast.LENGTH_LONG).show()
        
        # Read the application's preferences.
        self.preferences = self.getSharedPreferences("WeatherForecast",
                                                     self.MODE_PRIVATE)
        self.restorePreferences()
    
    def restorePreferences(self):
    
        self.configureWidget.restore(self.preferences)
    
    def savePreferences(self):
    
        editor = self.preferences.edit()
        self.configureWidget.save(editor)
        editor.commit()
    
    def onPause(self):
    
        Activity.onPause(self)
        self.entryWidget.writeLocations()
        self.savePreferences()
    
    def locationEntered(self, name, coordinates):
    
        if self.state == "fetching":
            return
        
        self.current_time = System.currentTimeMillis()
        self.place = coordinates
        self.place_name = name
        
        try:
            item = self.cache[coordinates]
            if self.current_time - item.time < 3600000: # 60 minutes
                self.showForecasts(item.forecasts, "")
                return
        
        except KeyError:
            pass
        
        self.state = "fetching"
        
        # Normal operation:
        self.task = Task(self, self.socketFactory)
        self.task.execute(array([coordinates]))
        
        # Testing using sample data:
        #forecasts = Forecasts(self.parser.parse(self.getSampleStream()))
        #self.showForecasts(forecasts.forecasts, "Invalid sample input")
    
    @args(void, [List(Forecast), str])
    def showForecasts(self, forecasts, errorMessage):
    
        if len(forecasts) == 0:
            self.showError(errorMessage)
            self.state = "entry"
            return
        
        self.cache[self.place] = CacheItem(self.current_time, forecasts)
        
        try:
            self.forecastWidget = ForecastWidget(self)
            self.forecastWidget.restore(self.preferences)
            self.forecastWidget.addForecasts(self.place_name, forecasts, self.preferences)
            self.forecastWidget.detailedForecastBox.setOnClickListener(self)
            
            self.state = "forecast"
            self.setContentView(self.forecastWidget)
        
        except:
            self.state = "entry"
            self.showError("")
    
    @args(void, [str])
    def showError(self, errorMessage):
    
        if errorMessage != "":
            Log.w("DUCK", errorMessage)
            Toast.makeText(self, errorMessage, Toast.LENGTH_LONG).show()
        else:
            Toast.makeText(self, R.string.failed_to_read,
                           Toast.LENGTH_SHORT).show()
    
    @args(InputStream, [])
    def getSampleStream(self):
    
        resources = self.getResources()
        return resources.openRawResource(R.raw.sample)
    
    def startConfiguration(self):
    
        self.state = "configure"
        self.setContentView(self.configureWidget)
    
    def finishConfiguration(self, save):
    
        if save:
            self.savePreferences()
        
        self.restorePreferences()
        self.state = "entry"
        self.setContentView(self.entryWidget)
    
    def onBackPressed(self):
    
        if self.state == "forecast":
            # Return to the entry widget.
            self.state = "entry"
            self.setContentView(self.entryWidget)
        
        elif self.state == "configure":
            # Directly call the interface method in this class.
            self.finishConfiguration(True)
        
        elif self.state == "entry":
            # Already showing the entry widget.
            if self.entryWidget.mode != "menu":
                self.entryWidget.enterMenuMode()
            else:
                # Exit if already showing the menu.
                Activity.onBackPressed(self)
    
    def onClick(self, view):
    
        if view == self.forecastWidget.detailedForecastBox:
            box = self.forecastWidget.detailedForecastBox
            
            editor = self.preferences.edit()
            if box.isChecked():
                editor.putInt("forecast period", 1)
            else:
                editor.putInt("forecast period", 6)
            editor.commit()
            
            self.showForecasts(self.forecastWidget.forecasts, "")


class Task(AsyncTask):

    #               Params Progress Result
    __item_types__ = [Coordinates, int, Forecasts]
    
    @args(void, [WeatherForecastActivity, TLSSocketFactory])
    def __init__(self, activity, socketFactory):
    
        AsyncTask.__init__(self)
        self.activity = activity
        self.socketFactory = socketFactory
        self.errorMessage = ""
    
    @args(Result, [[Params]])
    def doInBackground(self, params):
    
        # Unpack the location from the array.
        location = params[0]
        
        try:
            forecasts = self.fetchData(location)
        except Exception, e:
            self.errorMessage = e.getMessage()
            return Forecasts([])    # the compiler incorrectly allows [] here
        
        return forecasts
    
    @args(Forecasts, [Coordinates])
    def fetchData(self, coordinates):
    
        # Try to send an HTTPS request even if we have already warned about the
        # lack of support when the application started. Sending an HTTP request
        # will fail due to a 301 Moved Permanently response.
        url = URL("https://api.met.no/weatherapi/locationforecast/2.0/compact?" + \
              "altitude=" + coordinates.altitude + \
              "&lat=" + coordinates.latitude + \
              "&lon=" + coordinates.longitude)
        
        connection = CAST(url.openConnection(), HttpURLConnection)
        connection.setInstanceFollowRedirects(True)
        connection.setRequestProperty("User-Agent", "uk.org.boddie.android.weatherforecast")
        connection.setRequestProperty("Accept", "application/json")
        
        try:
            stream = BufferedInputStream(connection.getInputStream())
        except:
            # Return an empty list of forecasts.
            return Forecasts()
        
        forecasts = Forecasts(self.activity.parser.parse(stream))
        stream.close()
        
        return forecasts
    
    @args(void, [Result])
    def onPostExecute(self, forecasts):
    
        self.activity.showForecasts(forecasts.forecasts, self.errorMessage)


class CacheItem(Object):

    __fields__ = {"time": long, "forecasts": List(Forecast)}
    
    @args(void, [long, List(Forecast)])
    def __init__(self, time, forecasts):
    
        Object.__init__(self)
        
        self.time = time
        self.forecasts = forecasts
